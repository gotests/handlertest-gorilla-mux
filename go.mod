module gitlab.com/gotests/handlertest-gorilla-mux

go 1.13

require gitlab.com/gotests/handlertest v1.1.0

require github.com/gorilla/mux v1.7.4
