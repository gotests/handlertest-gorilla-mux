package handlertest_gorilla_mux_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/gotests/handlertest"
)

func Example_inject_vars() {
	PrintMuxVar := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		fmt.Printf("%v", vars)
	}

	t := new(testing.T)

	handlertest.Call(PrintMuxVar).Request(func(r *http.Request) *http.Request {
		return mux.SetURLVars(r, map[string]string{
			"key": "value",
		})
	}).Assert(t)

	// Output: map[key:value]
}

func Example_test_routes() {
	PrintMuxVar := func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		fmt.Printf("%v", vars)
	}

	t := new(testing.T)

	router := mux.NewRouter()
	router.HandleFunc("/products/{key}", PrintMuxVar)

	handlertest.Call(router.ServeHTTP).GET("/products/library").
		Assert(t)

	// Output: map[key:library]
}
